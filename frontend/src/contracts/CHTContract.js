import { ethers } from 'ethers';
import abi from './abi'


class CHTContract {
  constructor() {

    const provider = new ethers.providers.Web3Provider(window.web3.currentProvider);
    const signer = provider.getSigner();
    this._contractAddress = "0x6e979361150f0769fa7F2c424304495b0299A9A3";
    this._contractInstance = new ethers.Contract(this._contractAddress, abi, signer);
    this._txOption = {
      // The maximum units of gas for the transaction to use
      gasLimit: 223000,
      // The chain ID (or network ID) to use
      chainId: 10,
      gasPrice: 0
    };
  }

  get contractAddress() {
    return this._contractAddress;
  }

  get web3() {
    return this._web3;
  }

  formatAddress(address) {
    return this._web3.utils.asciiToHex(address).substring(0, 30);
  }

  // newHarvest(address grower, string memory ipfsHashNewHarvest)
  async newHarvest(growerAddress, ipfsHashNewHarvest) {
    try {
      console.log("newHarvest method", growerAddress, ipfsHashNewHarvest)
      let tx = await this._contractInstance.newProductItem(
        growerAddress,
        ipfsHashNewHarvest, 
        this._txOption
      );
      console.log("tx", tx);
      let receipt = await tx.wait();
      console.log("receipt", receipt);
      let event = receipt.events.pop()
      console.log("event", event)
      const newId = ethers.utils.bigNumberify(event.data).toNumber()
      console.log("newId", newId)
      return newId;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
  // function registerSample(uint256 cropId, string memory sampleJsonIpfsHash)
  async registerSample(cropId, ipfsHashSample) {
    try {

      let tx = await this._contractInstance.registerMetadata(
        cropId,
        ipfsHashSample, 
        this._txOption
      );
      await tx.wait();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
  async approveLaboratory(laboratoryAddress, cropId) {
    try {

      let tx = await this._contractInstance.approve(laboratoryAddress, cropId, this._txOption);
      await tx.wait();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
}
export default CHTContract;