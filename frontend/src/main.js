import Vue from 'vue'
import App from './App.vue'

import './assets/css/style.css';

Vue.config.productionTip = false


import IPFS from 'ipfs'

Vue.prototype.$ipfs = IPFS.create({})

// ------------
// custom components
// ------------
import Icon from '@/components/common/Icon';
Vue.component('Icon', Icon);

// ------------
// Layouts
// ------------
import Default from './layouts/Default.vue';
Vue.component('default-layout', Default);

import router from './router';

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
