# Quorum Medical Tracking

## How it works

QMT allows FDA and State Drugs agencies to supervise controlled medicine testing, transportation and commercialization. The Medicine laboratories, factories and resellers are allowed to access the permissioned Blockchain network and input data that is automatically shared among agencies and stakeholders nodes.

The Medicine movement details are registered following a metadata pattern and stored in a private IPFS network.

### Smart Contract use case

1. FDA allows laboratories to registers information into the smart contract.
2. Medicine Factories/Producers include new production batches and random submissions of a sample amount to test validations.
3. Medicine Factories/Producers can register when a specific batch is sent to a reseller.

## Why Quorum?

As it deals with sensitive information Quorum is the fast and cheap solution to share encrypted data among trustworthy partners. Besides node's infra costs there are no more issues to goverment agencies to  receive and share data. Saving a lot of tax payers's money in extra integration systems and complicated softwares.

## Items to check

Please visit /tracker-smartcontracts/smartcontracts/cmt.sol and /tracker-smartcontracts/smartcontracts/cmt-metadata-template.json

Also please build locally out VUE application in order to see page's details.